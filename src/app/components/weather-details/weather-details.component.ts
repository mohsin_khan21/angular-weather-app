import { Component, OnInit, Input } from '@angular/core';
import { Weather } from '../../model/weather.model'

@Component({
  selector: 'weather-details',
  templateUrl: './weather-details.component.html',
  styleUrls: ['./weather-details.component.css']
})
export class WeatherDetailsComponent implements OnInit {

  @Input() currentWeather : Weather
  imgIcon : string
  constructor() { }

  ngOnInit(): void {
    this.imgIcon = "http://openweathermap.org/img/w/"+ this.currentWeather.weather[0].icon +".png"
  }

}
