export class Weather {
    name : string;
    main : {
        temp : Number,
        humidity : Number,
        pressure : Number,
        feels_like : Number
    };
    coord : {
        lon : Number,
        lat : Number
    };
    weather : [
        {
            icon : String
        }
    ]
}
