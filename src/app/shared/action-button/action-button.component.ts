import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'action-button',
  templateUrl: './action-button.component.html',
  styleUrls: ['./action-button.component.css']
})
export class ActionButtonComponent implements OnInit {

  @Input() value : string
  @Input() class : string
  @Input() formValue : string
  @Output() navigateTo = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }

  navigation_to(){
    this.navigateTo.emit(this.formValue)
  }

}
