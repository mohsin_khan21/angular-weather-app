import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  constructor(
    private http: HttpClient,
    public router: Router
    ) { }

  getWeather(search):Observable<any>{
    let endpoint = "http://api.openweathermap.org/data/2.5/weather?q="+search+"&units=metric&APPID=ef8786380fda865218e279436d1f19f1"

    return this.http.get<any>(endpoint).pipe(
      map((res: Response) => {
        return res || {}
      }), catchError(err=>{
        return throwError(err)
      })
    )
  }
}
