import { NgModule } from '@angular/core';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {WeatherComponent} from './weather.component';
import {WeatherRoutingModule} from './weather-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {WeatherDetailsComponent} from '../../components/weather-details/weather-details.component'

@NgModule({
  declarations: [
    WeatherComponent,
    WeatherDetailsComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    WeatherRoutingModule,
    SharedModule
  ],
  exports:[WeatherComponent]
})
export class WeatherModule { }
