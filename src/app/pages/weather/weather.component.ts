import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../../services/weather.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-weather',
  templateUrl: 'weather.component.html',
  styleUrls: ['weather.component.css']
})
export class WeatherComponent implements OnInit {
  public weatherSearchForm: FormGroup;

  currentWeather: any
  _isData : boolean
  _isError : boolean
  imgIcon : string
  btnClass :string = 'btn btn-success btn-md'
  btnText :string = 'Search'

  constructor(private weatherservice : WeatherService, private formBuilder: FormBuilder) { 
    this._isData = false
    this._isError = true
  }

  ngOnInit(): void {
    this.weatherSearchForm = this.formBuilder.group({
      location: ['']
    });
  }

  weatherFormSerach(formValue){
    this._isData = false
    this.weatherservice.getWeather(formValue.location).subscribe(res => {
      this._isData = true
      this.currentWeather = res
      
    }, err=>{
      this._isError = false
    }, ()=>{
      
    })
  }

}
