import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-test';
  btnText = 'Enter to Weather App'
  btnClassPrimary = 'btn btn-primary'
  _isShow = true

  constructor(private _router: Router){}

  onClick(){
    this._isShow = false
    this._router.navigate(['weather'])
  }
}
